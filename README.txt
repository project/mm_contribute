
********************************************************************
                     D R U P A L    M O D U L E                         
********************************************************************
Name: mm_contribute
Author: Simon Hobbs <info at urbits dot com>
Drupal: CVS
Module Dependencies: none

********************************************************************
DESCRIPTION:

Macromedia Contribute is a website management tool with a WYSIWYG
editor and permissions management. It is nice (but not unique) 
in that you can force the user to choose styles from a specific
style sheet and you can hide your php snippets and other stuff.
This module allows you to let your users continue to use Contribute
while getting the full advantage Drupal.

If your customer is not already using Contribute then consider
one of the WYSIWYG editors within Drupal.

Nothing hugely special in the module itself, it actually is just a
tool to help your match a node with a html file. Most of the trick
is in how you set up Contribute. See the CONTRIBUTE.txt file for
details and examples.


********************************************************************
INSTALLATION:

1. Copy the mm_contribute folder to the the modules folder of the website.
2. Go to home->administer->modules (?q=admin/modules) and check "mm_contribute"
   and [Save Configuration]
3. Go to home->administer->settings->mm_contribute (?q=admin/settings/mm_contribute)
   and enter the folder which will contain the contribute files.
   
NB. You should have the Contribute directory (with the Contribute-created
		html files) already created before Step 3. Look at CONTRIBUTE.txt if you have
		no idea what I'm talking about.

********************************************************************

20060330 - parsing by custom tag (default is body)
         - careful parsing using regex (not recommended unless very bad markup)
         - now stores the parsed content in table. checks the source file
           timestamp and updates the table if different
           